import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { AppService } from './app.service';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { DevilComponent } from './devil/devil.component';
import { HellComponent } from './hell/hell.component';
import { DemoComponent } from './demo/demo.component';
import { KaranComponent } from './karan/karan.component';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    AdminComponent,
    LoginComponent,
    DevilComponent,
    HellComponent,
    DemoComponent,
    KaranComponent,
    DemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
