import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevilComponent } from './devil.component';

describe('DevilComponent', () => {
  let component: DevilComponent;
  let fixture: ComponentFixture<DevilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
