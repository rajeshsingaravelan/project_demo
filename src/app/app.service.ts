import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import {App} from './app.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
serverUrl = 'http://localhost/codeang/index.php/';

  constructor(private http: HttpClient) { }

  public get()
  {
      return this.http.get<any>(this.serverUrl + 'api/get');
  }
  public get_id(id:number)
  {
      return this.http.get<any>(this.serverUrl + 'api/get_id/' + id);
  }

public createreg(data)
{
    var header:HttpHeaders = new HttpHeaders();
    header.append('Content-Type','application/json');
    return this.http.post<any>(this.serverUrl + 'api/createreg',data, {headers: header });
}
public createadmin(data)
{
    var header:HttpHeaders = new HttpHeaders();
    header.append('Content-Type','application/json');
    return this.http.post<any>(this.serverUrl + 'api/createadmin',data, {headers: header });
}
public delete(id:number)
{

    return this.http.get(this.serverUrl + 'api/delete_obj/' + id);
  }

public update(id:number, data:any)
{
      //data.id = id
      return this.http.post<any>(this.serverUrl + 'api/update/',data);


    }
    public loginget(data)
    {
      var header:HttpHeaders = new HttpHeaders();
      header.append('Content-Type','application/json');
        return this.http.post<any>(this.serverUrl + 'api/validate_login',data,{headers: header });
    }
}
