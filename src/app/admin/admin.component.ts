import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import {App} from '../app.model';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  admins:Array<any> = [];
  admin:any={};
  constructor(private appservice:AppService) { }

  ngOnInit() {
  }
  onsubmit()
  {
  this.appservice.createadmin(this.admin).subscribe((res) => {

    console.log(res);
  
  }
  );

  console.log("sucess");
  }
}
